package ru.amster.tm.exception.system;

import ru.amster.tm.exception.AbstractException;

public class InvalidIndexException extends AbstractException {

    public InvalidIndexException(String value) {
        super("Error! This ''" + value + "'' is not number...");
    }

    public InvalidIndexException(Integer index, Integer maxIndex) {
        super("ERROR! Index ''" + (index + 1) + "'' more than items in the collection ''" + maxIndex + "''");
    }

    public InvalidIndexException() {
        super("Error! Index incorrect...");
    }

}