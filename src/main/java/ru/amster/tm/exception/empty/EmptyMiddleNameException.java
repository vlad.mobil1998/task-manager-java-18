package ru.amster.tm.exception.empty;

import ru.amster.tm.exception.AbstractException;

public class EmptyMiddleNameException extends AbstractException {

    public EmptyMiddleNameException() {
        super("Error! Middle name is empty...");
    }

}