package ru.amster.tm.repository;

import ru.amster.tm.api.repository.ITaskRepository;
import ru.amster.tm.entity.Task;
import ru.amster.tm.exception.empty.EmptyTaskException;
import ru.amster.tm.exception.user.AccessDeniedException;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    public List<Task> getTasks() {
        return tasks;
    }

    @Override
    public void add(final String userId, final Task task) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        task.setUserId(userId);
        tasks.add(task);
    }

    @Override
   public void removeAll() {
        tasks.clear();
    }

    @Override
    public void remove(final String userId, final Task task) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (userId.equals(task.getUserId())) tasks.remove(task);
    }

    @Override
    public List<Task> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final List<Task> result = new ArrayList<>();
        for (final Task task : tasks) {
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result;
    }

    @Override
    public void clear(final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        for (final Task task : tasks) {
            if (!userId.equals(task.getUserId())) continue;
            remove(userId, task);
            return;
        }
    }

    @Override
    public Task findOneByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        Task result = null;
        for (final Task task : tasks) {
            if (!name.equals(task.getName())) continue;
            result = task;
            break;
        }
        if (result == null) throw new EmptyTaskException();
        if (!userId.equals(result.getUserId())) throw new AccessDeniedException();
        return result;
    }

    @Override
    public Task findOneByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        Task task = tasks.get(index);
        if (task == null) throw new EmptyTaskException();
        if (!userId.equals(task.getUserId())) throw new AccessDeniedException();
        return task;
    }

    @Override
    public Task findOneById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        Task result = null;
        for (final Task task : tasks) {
            if (!id.equals(task.getId())) continue;
            result = task;
            break;
        }
        if (result == null) throw new EmptyTaskException();
        if (!userId.equals(result.getUserId())) throw new AccessDeniedException();
        return result;
    }

    @Override
    public Task removeOneByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final Task task = findOneByName(userId, name);
        if (task == null) throw new EmptyTaskException();
        if (!userId.equals(task.getUserId())) throw new AccessDeniedException();
        remove(userId, task);
        return task;
    }

    @Override
    public Task removeOneByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final Task task = findOneByIndex(userId, index);
        if (task == null) throw new EmptyTaskException();
        if (!userId.equals(task.getUserId())) throw new AccessDeniedException();
        remove(userId, task);
        return task;
    }

    @Override
    public Task removeOneById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final Task task = findOneById(userId, id);
        if (task == null) throw new EmptyTaskException();
        if (!userId.equals(task.getUserId())) throw new AccessDeniedException();
        remove(userId, task);
        return task;
    }

    @Override
    public Integer numberOfAllTasks(final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        List<Task> result = findAll(userId);
        return result.size();
    }

}