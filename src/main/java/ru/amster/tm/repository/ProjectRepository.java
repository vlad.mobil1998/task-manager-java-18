package ru.amster.tm.repository;

import ru.amster.tm.api.repository.IProjectRepository;
import ru.amster.tm.entity.Project;
import ru.amster.tm.exception.empty.EmptyProjectException;
import ru.amster.tm.exception.user.AccessDeniedException;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    public List<Project> getProjects() {
        return projects;
    }

    @Override
    public void add(final String userId, final Project project) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        project.setUserId(userId);
        projects.add(project);
    }

    public void removeAll() {
        projects.clear();
    }

    @Override
    public void remove(final String userId, final Project project) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (userId.equals(project.getUserId())) projects.remove(project);
    }

    @Override
    public List<Project> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final List<Project> result = new ArrayList<>();
        for (final Project project : projects) {
            if (userId.equals(project.getUserId())) result.add(project);
        }
        return result;
    }

    @Override
    public void clear(final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        for (final Project project : projects) {
            if (!userId.equals(project.getUserId())) continue;
            remove(userId, project);
            return;
        }
    }

    @Override
    public Project findOneById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        Project result = null;
        for (final Project project : projects) {
            if (!id.equals(project.getId())) continue;
            result = project;
            break;
        }
        if (result == null) throw new EmptyProjectException();
        if (!userId.equals(result.getUserId())) throw new AccessDeniedException();
        return result;
    }

    @Override
    public Project findOneByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        Project result = null;
        for (final Project project : projects) {
            if (!name.equals(project.getName())) continue;
            result = project;
            break;
        }
        if (result == null) throw new EmptyProjectException();
        if (!userId.equals(result.getUserId())) throw new AccessDeniedException();
        return result;
    }

    @Override
    public Project findOneByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        Project project = projects.get(index);
        if (project == null) throw new EmptyProjectException();
        if (!userId.equals(project.getUserId())) throw new AccessDeniedException();
        return project;
    }

    @Override
    public Project removeOneByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final Project project = findOneByName(userId, name);
        if (project == null) throw new EmptyProjectException();
        if (!userId.equals(project.getUserId())) throw new AccessDeniedException();
        remove(userId, project);
        return project;
    }

    @Override
    public Project removeOneByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final Project project = findOneByIndex(userId, index);
        if (project == null) throw new EmptyProjectException();
        if (!userId.equals(project.getUserId())) throw new AccessDeniedException();
        remove(userId, project);
        return project;
    }

    @Override
    public Project removeOneById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final Project project = findOneById(userId, id);
        if (project == null) throw new EmptyProjectException();
        if (!userId.equals(project.getUserId())) throw new AccessDeniedException();
        remove(userId, project);
        return project;
    }

    @Override
    public Integer numberOfAllProjects(final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        List<Project> result = findAll(userId);
        return result.size();
    }

}