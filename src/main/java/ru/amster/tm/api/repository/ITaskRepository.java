package ru.amster.tm.api.repository;

import ru.amster.tm.entity.Task;

import java.util.List;

public interface ITaskRepository {

    List<Task> getTasks();

    void add(String userId, Task task);

    void remove(String userId, Task task);

    void removeAll();

    List<Task> findAll(String userId);

    void clear(String userId);

    Task findOneById(String userId, String id);

    Task findOneByName(String userId, String name);

    Task findOneByIndex(String userId, Integer index);

    Task removeOneByName(String userId, String name);

    Task removeOneByIndex(String userId, Integer index);

    Task removeOneById(String userId, String id);

    Integer numberOfAllTasks(String userId);
}