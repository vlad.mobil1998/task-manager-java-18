package ru.amster.tm.api.repository;

import ru.amster.tm.entity.User;

import java.util.List;

public interface IUserRepository {

    List<User> getUsers();

    List<User> findAll();

    User add(User user);

    User findById(String id);

    User findByLogin(String login);

    User removeUser(User user);

    User removeById(String id);

    User removeByLogin(String login);

}