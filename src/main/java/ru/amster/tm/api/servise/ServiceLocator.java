package ru.amster.tm.api.servise;

public interface ServiceLocator {

    IAuthenticationService getAuthService();

    IUserService getUserService();

    ITaskService getTaskService();

    IProjectService getProjectService();

    IDomainService getDomainService();

}