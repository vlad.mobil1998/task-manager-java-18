package ru.amster.tm.api.servise;

import ru.amster.tm.entity.Project;

import java.util.List;

public interface IProjectService {

    void create(String userId, String name);

    void create(String userId, String name, String description);

    void add(String userId, Project project);

    void addAll(Project project);

    void clear();

    void remove(String userId, Project project);

    List<Project> findAll(String userId);

    void clear(String userId);

    Project findOneById(String userId, String id);

    Project findOneByIndex(String userId, Integer index);

    Project findOneByName(String userId, String name);

    Project removeOneByName(String userId, String name);

    Project removeOneByIndex(String userId, Integer index);

    Project removeOneById(String userId, String id);

    Project updateProjectById(String userId, String id, String name, String description);

    Project updateProjectByIndex(String userId, Integer index, String name, String description);

    Integer numberOfAllProjects(String userId);

    void load(List<Project> projects);

    List<Project> export();

}