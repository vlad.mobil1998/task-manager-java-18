package ru.amster.tm.api.servise;

import ru.amster.tm.entity.Task;

import java.util.List;

public interface ITaskService {

    void create(String userId, String name);

    void create(String userId, String name, String description);

    void add(String userId, Task project);

    void remove(String userId, Task project);

    List<Task> findAll(String userId);

    void clear(String userId);

    Task findOneById(String userId, String id);

    Task findOneByIndex(String userId, Integer index);

    Task findOneByName(String userId, String name);

    Task removeOneByName(String userId, String name);

    Task removeOneByIndex(String userId, Integer index);

    Task removeOneById(String userId, String id);

    Task updateTaskById(String userId, String id, String name, String description);

    Task updateTaskByIndex(String userId, Integer index, String name, String description);

    Integer numberOfAllTasks(String userId);

    void load(List<Task> tasks);

    List<Task> export();

}