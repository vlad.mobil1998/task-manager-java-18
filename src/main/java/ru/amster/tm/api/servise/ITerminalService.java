package ru.amster.tm.api.servise;

import ru.amster.tm.command.AbstractCommand;

import java.util.Collection;

public interface ITerminalService extends ServiceLocator {

    Collection<AbstractCommand> getCommands();

    Collection<AbstractCommand> getArguments();

    AbstractCommand getCommand(final String cmd);

    AbstractCommand getArgument(final String arg);

    @Override
    IAuthenticationService getAuthService();

    @Override
    IUserService getUserService();

    @Override
    ITaskService getTaskService();

    @Override
    IProjectService getProjectService();

    @Override
    IDomainService getDomainService();

}