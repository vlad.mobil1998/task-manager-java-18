package ru.amster.tm.command.user.update;

import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.exception.empty.EmptyEmailException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TerminalUtil;

public class FirstNameUpdateCommand extends AbstractCommand {

    @Override
    public String name() {
        return "upd-first-name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return " - updating user first name";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE FIRST NAME]");
        System.out.println("ENTER FIRST NAME");
        final String userId = serviceLocator.getAuthService().getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final String firstName = TerminalUtil.nextLine();
        if (firstName == null || firstName.isEmpty()) throw new EmptyEmailException();
        serviceLocator.getUserService().updateFirstName(userId, firstName);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}