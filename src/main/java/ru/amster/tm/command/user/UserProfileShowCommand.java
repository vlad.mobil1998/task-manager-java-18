package ru.amster.tm.command.user;

import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.entity.User;
import ru.amster.tm.exception.empty.EmptyUserException;
import ru.amster.tm.exception.user.AccessDeniedException;

public class UserProfileShowCommand extends AbstractCommand {

    @Override
    public String name() {
        return "view-profile";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return " - Show you profile";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        User user = serviceLocator.getUserService().findById(userId);
        if (user == null) throw new EmptyUserException();
        System.out.println(user.getLogin());
        if (user.getEmail() != null) System.out.println(user.getEmail());
        if (user.getFistName() != null) System.out.println(user.getFistName());
        if (user.getMiddleName() != null) System.out.println(user.getMiddleName());
        if (user.getLastName() != null) System.out.println(user.getLastName());
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}