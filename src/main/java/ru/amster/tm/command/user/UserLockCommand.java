package ru.amster.tm.command.user;

import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.util.TerminalUtil;

public class UserLockCommand extends AbstractCommand {

    @Override
    public String name() {
        return "lock-user";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return " - Lock user by login";
    }

    @Override
    public void execute() {
        System.out.println("[LOCK USER]");
        System.out.println("ENTER LOGIN");
        final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().lockUserByLogin(login);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}