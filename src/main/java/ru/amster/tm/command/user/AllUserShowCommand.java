package ru.amster.tm.command.user;

import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.entity.User;
import ru.amster.tm.enamuration.Role;

import java.util.List;

public class AllUserShowCommand extends AbstractCommand {

    @Override
    public String name() {
        return "view-all-users";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return " - Show All Users";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW ALL USER]");
        final List<User> users = serviceLocator.getUserService().findAll();
        for (User item : users) {
            System.out.println(item.getLogin());
            if (item.getEmail() != null) System.out.println(item.getEmail());
            if (item.getFistName() != null) System.out.println(item.getFistName());
            if (item.getMiddleName() != null) System.out.println(item.getMiddleName());
            if (item.getLastName() != null) System.out.println(item.getLastName());
            System.out.println(" ");
        }
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}