package ru.amster.tm.command.project;

import ru.amster.tm.api.servise.IProjectService;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.entity.Project;
import ru.amster.tm.exception.empty.EmptyIdException;
import ru.amster.tm.exception.empty.EmptyNameException;
import ru.amster.tm.exception.empty.EmptyProjectException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TerminalUtil;

public class ProjectUpdateByIdCommand extends AbstractCommand {

    @Override
    public String name() {
        return "project-upd-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return " - Update project by id";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER ID");
        final String userId = serviceLocator.getAuthService().getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final String id = TerminalUtil.nextLine();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Project project = serviceLocator.getProjectService().findOneById(userId, id);
        if (project == null) throw new EmptyProjectException();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        System.out.println("ENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        final IProjectService projectService = serviceLocator.getProjectService();
        final Project projectUpdate = projectService.updateProjectById(userId, id, name, description);
        if (projectUpdate == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}