package ru.amster.tm.command.data.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.amster.tm.command.data.AbstractDataCommand;
import ru.amster.tm.dto.Domain;
import ru.amster.tm.enamuration.Role;

import java.io.*;
import java.nio.file.Files;

public class DataJsonSaveCommand extends AbstractDataCommand {

    @Override
    public String name() {
        return "data-json-save";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return " - Save data from json file";
    }

    @Override
    public void execute() throws IOException, ClassNotFoundException {
        System.out.println("[DATA JSON SAVE]");
        final Domain domain = getDomain();

        final File file = new File(FILE_JSON);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        final ObjectMapper objectMapper = new ObjectMapper();
        final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);

        final FileOutputStream fileOutputStream = new FileOutputStream(FILE_JSON);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();

        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}