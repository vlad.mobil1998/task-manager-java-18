package ru.amster.tm.command.data.xml;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.amster.tm.command.data.AbstractDataCommand;
import ru.amster.tm.dto.Domain;
import ru.amster.tm.enamuration.Role;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class DataXmlLoadCommand extends AbstractDataCommand {

    @Override
    public String name() {
        return "data-xml-load";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return " - Load xml data to file";
    }

    @Override
    public void execute() throws IOException, ClassNotFoundException {
        System.out.println("[DATA XML LOAD]");

        File file = new File(FILE_XML);
        final FileInputStream fileInputStream = new FileInputStream(file);

        ObjectMapper objectMapper = new XmlMapper();
        Domain domain = objectMapper.readValue(file, Domain.class);

        setDomain(domain);
        fileInputStream.close();

        serviceLocator.getAuthService().logout();
        userActivated.setCheckForActivation(false);

        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}