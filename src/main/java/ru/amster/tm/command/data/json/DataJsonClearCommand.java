package ru.amster.tm.command.data.json;

import ru.amster.tm.command.data.AbstractDataCommand;
import ru.amster.tm.enamuration.Role;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class DataJsonClearCommand extends AbstractDataCommand {

    @Override
    public String name() {
        return "data-json-clear";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return " - Remove json data";
    }

    @Override
    public void execute() throws IOException, ClassNotFoundException {
        System.out.println("[DATA JSON CLEAR]");
        final File file = new File(FILE_JSON);
        Files.delete(file.toPath());
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}