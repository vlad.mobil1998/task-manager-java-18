package ru.amster.tm.command.data.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.amster.tm.command.data.AbstractDataCommand;
import ru.amster.tm.dto.Domain;
import ru.amster.tm.enamuration.Role;
import sun.misc.BASE64Decoder;

import java.io.*;

public class DataJsonLoadCommand extends AbstractDataCommand {

    @Override
    public String name() {
        return "data-json-load";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return " - Load json data to file";
    }

    @Override
    public void execute() throws IOException, ClassNotFoundException {
        System.out.println("[DATA JSON LOAD]");

        File file = new File(FILE_JSON);
        final FileInputStream fileInputStream = new FileInputStream(file);

        ObjectMapper objectMapper = new ObjectMapper();
        Domain domain = objectMapper.readValue(file, Domain.class);

        setDomain(domain);
        fileInputStream.close();

        serviceLocator.getAuthService().logout();
        userActivated.setCheckForActivation(false);

        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}