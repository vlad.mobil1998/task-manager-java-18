package ru.amster.tm.command.system;

import ru.amster.tm.command.AbstractCommand;

public class SystemExitCommand extends AbstractCommand {

    @Override
    public String name() {
        return "exit";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return " - Close application.";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}