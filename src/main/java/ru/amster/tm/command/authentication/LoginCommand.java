package ru.amster.tm.command.authentication;

import ru.amster.tm.enamuration.Role;
import ru.amster.tm.entity.User;
import ru.amster.tm.exception.empty.EmptyLoginException;
import ru.amster.tm.exception.empty.EmptyPasswordException;
import ru.amster.tm.exception.empty.EmptyUserException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TerminalUtil;

public class LoginCommand extends AbstractAuthCommand {

    @Override
    public String name() {
        return "login";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return " - Sign in system";
    }

    @Override
    public void execute() {
        System.out.println("[LOGIN]");
        if (userActivated.getCheckForActivation()) throw new AccessDeniedException();
        System.out.println("[ENTER LOGIN]");
        final String login = TerminalUtil.nextLine();
        User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) throw new EmptyUserException();
        if (user.getLocked()) throw new AccessDeniedException();
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        System.out.println("[ENTER PASSWORD]");
        final String password = TerminalUtil.nextLine();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        serviceLocator.getAuthService().login(login, password);
        userActivated.setCheckForActivation(true);
        System.out.println("[OK]");
    }

}