package ru.amster.tm.service;

import ru.amster.tm.api.repository.IUserRepository;
import ru.amster.tm.api.servise.IUserService;
import ru.amster.tm.entity.User;
import ru.amster.tm.exception.empty.*;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.util.HashUtil;

import java.util.List;

public class UserService implements IUserService {

    private final IUserRepository userRepository;

    public UserService(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User create(final String login, final String password, Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(role);
        userRepository.add(user);
        return user;
    }

    @Override
    public User create(final String login, final String password, final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        final User user = new User();
        if (user == null) throw new EmptyUserException();
        user.setLogin(login);
        user.setEmail(email);
        user.setPasswordHash(HashUtil.salt(password));
        userRepository.add(user);
        return user;
    }

    @Override
    public User create(final String login, final String password, final String email, Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (role == null) throw new EmptyRoleException();
        final User user = new User();
        if (user == null) throw new EmptyUserException();
        user.setLogin(login);
        user.setEmail(email);
        user.setRole(role);
        user.setPasswordHash(HashUtil.salt(password));
        userRepository.add(user);
        return user;
    }

    @Override
    public User findById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.findById(id);
    }

    @Override
    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    public User removeUser(final User user) {
        if (user == null) throw new EmptyUserException();
        return userRepository.removeUser(user);
    }

    @Override
    public User removeById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.removeById(id);
    }

    @Override
    public User removeByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.removeByLogin(login);
    }

    @Override
    public User updateEmail(final String id, final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        final User user = findById(id);
        if (user == null) throw new EmptyUserException();
        user.setEmail(email);
        return user;
    }

    @Override
    public User updateFirstName(final String id, final String firstName) {
        if (firstName == null || firstName.isEmpty()) throw new EmptyFirstNameException();
        final User user = findById(id);
        if (user == null) throw new EmptyUserException();
        user.setFistName(firstName);
        return user;
    }

    @Override
    public User updateLastName(final String id, final String lastName) {
        if (lastName == null || lastName.isEmpty()) throw new EmptyLastNameException();
        final User user = findById(id);
        if (user == null) throw new EmptyUserException();
        user.setLastName(lastName);
        return user;
    }

    @Override
    public User updateMiddleName(final String id, final String middleName) {
        if (middleName == null || middleName.isEmpty()) throw new EmptyMiddleNameException();
        final User user = findById(id);
        if (user == null) throw new EmptyUserException();
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public User updatePassword(final String id, final String password) {
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = findById(id);
        if (user == null) throw new EmptyUserException();
        final String passwordUpd = HashUtil.salt(password);
        user.setPasswordHash(passwordUpd);
        return user;
    }

    @Override
    public User lockUserByLogin(String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final User user = findByLogin(login);
        if (user == null) throw new EmptyUserException();
        user.setLocked(true);
        return user;
    }

    @Override
    public User unlockUserByLogin(String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final User user = findByLogin(login);
        if (user == null) throw new EmptyUserException();
        user.setLocked(false);
        return user;
    }

    @Override
    public void load(List<User> users) {
        User tempUser;
        for (User user: users) {
            tempUser = userRepository.findByLogin(user.getLogin());
            if (tempUser != null) tempUser.setId(user.getId());
            else userRepository.add(user);
        }
    }

    @Override
    public List<User> export() {
        return userRepository.getUsers();
    }

}