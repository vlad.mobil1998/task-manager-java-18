package ru.amster.tm.service;

import ru.amster.tm.api.repository.IProjectRepository;
import ru.amster.tm.api.repository.ITaskRepository;
import ru.amster.tm.api.repository.IUserRepository;
import ru.amster.tm.api.servise.*;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.command.authentication.*;
import ru.amster.tm.command.data.base64.DataBase64ClearCommand;
import ru.amster.tm.command.data.base64.DataBase64LoadCommand;
import ru.amster.tm.command.data.base64.DataBase64SaveCommand;
import ru.amster.tm.command.data.binary.DataBinaryClearCommand;
import ru.amster.tm.command.data.binary.DataBinaryLoadCommand;
import ru.amster.tm.command.data.binary.DataBinarySaveCommand;
import ru.amster.tm.command.data.json.DataJsonClearCommand;
import ru.amster.tm.command.data.json.DataJsonLoadCommand;
import ru.amster.tm.command.data.json.DataJsonSaveCommand;
import ru.amster.tm.command.data.xml.DataXmlClearCommand;
import ru.amster.tm.command.data.xml.DataXmlLoadCommand;
import ru.amster.tm.command.data.xml.DataXmlSaveCommand;
import ru.amster.tm.command.project.*;
import ru.amster.tm.command.system.*;
import ru.amster.tm.command.task.*;
import ru.amster.tm.command.user.*;
import ru.amster.tm.command.user.update.*;
import ru.amster.tm.repository.ProjectRepository;
import ru.amster.tm.repository.TaskRepository;
import ru.amster.tm.repository.UserRepository;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class TerminalService implements ITerminalService {

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthenticationService authService = new AuthenticationService(userService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IDomainService domainService = new DomainService(
            projectService, taskService, userService
    );

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    private final UserActivated userActivated = new UserActivated();

    private static final Class[] COMMANDS = new Class[]{
            SystemHelpCommand.class, SystemAboutCommand.class, SystemInfoCommand.class,
            SystemVersionCommand.class, SystemCommandCommand.class, SystemArgumentCommand.class,

            EmailUpdateCommand.class, FirstNameUpdateCommand.class, LastNameUpdateCommand.class,
            MiddleNameUpdateCommand.class, AllUserShowCommand.class, PasswordUpdateCommand.class,
            UserUnlockCommand.class, UserLockCommand.class, UserRemoveCommand.class, UserProfileShowCommand.class,

            ProjectCreateCommand.class, ProjectShowByIndexCommand.class, ProjectShowByNameCommand.class,
            ProjectShowByIdCommand.class, ProjectShowCommand.class, ProjectUpdateByIdCommand.class,
            ProjectUpdateByIndexCommand.class, ProjectRemoveByIdCommand.class,
            ProjectRemoveByNameCommand.class, ProjectRemoveByIndexCommand.class, ProjectClearCommand.class,

            TaskCreateCommand.class, TaskShowByIndexCommand.class, TaskShowByNameCommand.class,
            TaskShowByIdCommand.class, TaskShowCommand.class, TaskUpdateByIdCommand.class,
            TaskUpdateByIndexCommand.class, TaskRemoveByIdCommand.class, TaskRemoveByNameCommand.class,
            TaskRemoveByIndexCommand.class, TaskClearCommand.class,

            DataBinarySaveCommand.class, DataBinaryClearCommand.class,
            DataBase64SaveCommand.class, DataBase64ClearCommand.class,
            DataJsonSaveCommand.class, DataJsonClearCommand.class,
            DataXmlSaveCommand.class, DataXmlClearCommand.class,

            SystemExitCommand.class,
    };

    private static final Class[] COMMANDS_AUTH = new Class[]{
            LoginCommand.class, LogoutCommand.class, RegistrationCommand.class,
            DataBinaryLoadCommand.class, DataBase64LoadCommand.class,
            DataJsonLoadCommand.class, DataXmlLoadCommand.class,
    };

    private static final Class[] ARGUMENTS = new Class[]{
            SystemHelpCommand.class, SystemAboutCommand.class, SystemInfoCommand.class, SystemVersionCommand.class,
    };

    {
        for (final Class clazz : COMMANDS) {
            try {
                final Object commandInstance = clazz.newInstance();
                final AbstractCommand command = (AbstractCommand) commandInstance;
                command.setServiceLocator(this);
                commands.put(command.name(), command);
            } catch (Exception e) {
                throw new RuntimeException();
            }
        }
    }

    {
        for (final Class clazz : COMMANDS_AUTH) {
            try {
                final Object commandInstance = clazz.newInstance();
                final AbstractCommand command = (AbstractCommand) commandInstance;
                command.setServiceLocator(this);
                final AbstractAuthCommand commandAuth = (AbstractAuthCommand) command;
                commandAuth.setLoginCheck(userActivated);
                commands.put(command.name(), command);
            } catch (Exception e) {
                throw new RuntimeException();
            }
        }
    }

    {
        for (final Class clazz : ARGUMENTS) {
            try {
                final Object commandInstance = clazz.newInstance();
                final AbstractCommand argument = (AbstractCommand) commandInstance;
                arguments.put(argument.arg(), argument);
            } catch (Exception e) {
                throw new RuntimeException();
            }
        }
    }

    @Override
    public IAuthenticationService getAuthService() {
        return authService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IDomainService getDomainService() {
        return domainService;
    }

    @Override
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @Override
    public Collection<AbstractCommand> getArguments() {
        return arguments.values();
    }

    @Override
    public AbstractCommand getCommand(final String cmd) {
        return commands.get(cmd);
    }

    @Override
    public AbstractCommand getArgument(final String arg) {
        return arguments.get(arg);
    }

}