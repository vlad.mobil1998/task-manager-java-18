package ru.amster.tm.service;

import ru.amster.tm.api.repository.ITaskRepository;
import ru.amster.tm.api.servise.ITaskService;
import ru.amster.tm.entity.Task;
import ru.amster.tm.exception.empty.EmptyIdException;
import ru.amster.tm.exception.empty.EmptyNameException;
import ru.amster.tm.exception.empty.EmptyTaskException;
import ru.amster.tm.exception.system.InvalidIndexException;
import ru.amster.tm.exception.user.AccessDeniedException;

import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null && name.isEmpty()) throw new EmptyNameException();
        final Task task = new Task();
        task.setName(name);
        add(userId, task);
    }

    @Override
    public void create(final String userId, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null && name.isEmpty()) throw new EmptyNameException();
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        add(userId, task);
    }

    @Override
    public void add(final String userId, final Task task) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (task == null) throw new EmptyTaskException();
        taskRepository.add(userId, task);
    }

    @Override
    public List<Task> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        return taskRepository.findAll(userId);
    }

    @Override
    public Task findOneByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (index == null || index < 0) throw new InvalidIndexException();
        return taskRepository.findOneByIndex(userId, index);
    }

    @Override
    public Task findOneByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findOneByName(userId, name);
    }

    @Override
    public Task findOneById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.findOneById(userId, id);
    }

    @Override
    public Task removeOneByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.removeOneByName(userId, name);
    }

    @Override
    public Task removeOneByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (index == null || index < 0) throw new InvalidIndexException();
        return taskRepository.removeOneByIndex(userId, index);
    }

    @Override
    public Task removeOneById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.removeOneById(userId, id);
    }

    @Override
    public void remove(final String userId, final Task task) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (task == null) throw new EmptyTaskException();
        taskRepository.remove(userId, task);
    }

    @Override
    public Task updateTaskById(final String userId, final String id, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneById(userId, id);
        if (task == null) throw new EmptyTaskException();
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateTaskByIndex(final String userId, final Integer index, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (index == null || index < 0) throw new InvalidIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByIndex(userId, index);
        if (task == null) throw new EmptyTaskException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public void clear(final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        taskRepository.clear(userId);
    }

    @Override
    public Integer numberOfAllTasks(final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        return taskRepository.numberOfAllTasks(userId);
    }

    @Override
    public void load(List<Task> tasks) {
        taskRepository.removeAll();
        for (Task task: tasks) {
            add(task.getUserId(), task);
        }
    }

    public List<Task> export() {
        return taskRepository.getTasks();
    }

}