package ru.amster.tm.service;

import ru.amster.tm.api.repository.IProjectRepository;
import ru.amster.tm.api.servise.IProjectService;
import ru.amster.tm.entity.Project;
import ru.amster.tm.exception.empty.EmptyIdException;
import ru.amster.tm.exception.empty.EmptyNameException;
import ru.amster.tm.exception.empty.EmptyProjectException;
import ru.amster.tm.exception.system.InvalidIndexException;
import ru.amster.tm.exception.user.AccessDeniedException;

import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = new Project();
        project.setName(name);
        add(userId, project);
    }

    @Override
    public void create(final String userId, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) return;
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        add(userId, project);
    }

    @Override
    public void add(final String userId, Project project) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (project == null) throw new EmptyProjectException();
        projectRepository.add(userId, project);
    }

    @Override
    public void addAll(Project project) {

    }

    @Override
    public void clear() {

    }

    @Override
    public void remove(final String userId, final Project project) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (project == null) throw new EmptyProjectException();
        projectRepository.remove(userId, project);
    }

    @Override
    public List<Project> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        return projectRepository.findAll(userId);
    }

    @Override
    public void clear(final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        projectRepository.clear(userId);
    }

    @Override
    public Project findOneById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.findOneById(userId, id);
    }

    @Override
    public Project findOneByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (index == null || index < 0) throw new InvalidIndexException();
        return projectRepository.findOneByIndex(userId, index);
    }

    @Override
    public Project findOneByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findOneByName(userId, name);
    }

    @Override
    public Project removeOneByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.removeOneByName(userId, name);
    }

    @Override
    public Project removeOneByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (index == null || index < 0) throw new InvalidIndexException();
        return projectRepository.removeOneByIndex(userId, index);
    }

    @Override
    public Project removeOneById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.removeOneById(userId, id);
    }

    @Override
    public Project updateProjectById(final String userId, final String id, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneById(userId, id);
        if (project == null) throw new EmptyProjectException();
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateProjectByIndex(final String userId, final Integer index, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (index == null || index < 0) throw new InvalidIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneByIndex(userId, index);
        if (project == null) throw new EmptyProjectException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Integer numberOfAllProjects(final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        return projectRepository.numberOfAllProjects(userId);
    }

    @Override
    public void load(List<Project> projects) {
        projectRepository.removeAll();
        for (Project project: projects) {
            add(project.getUserId(), project);
        }
    }

    public List<Project> export() {
        return projectRepository.getProjects();
    }

}